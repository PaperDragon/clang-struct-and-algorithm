//
// Created by IT-Desktop on 2022/8/22.
//



#include "../include/stack_using_queues.h"


//typedef int QDataType;
//typedef struct QueueNode {
//    struct QueueNode *next;
//    QDataType data;
//}QueueNode;
//
//typedef struct {
//    QueueNode *head;
//    QueueNode *tail;
////    size_t size;
//}Queue;
//
//typedef struct {
//    Queue *q1;
//    Queue *q2;
//} MyStack;



void QueueInit(Queue *pq) {
    assert(pq);
    pq->head = NULL;
    pq->tail = NULL;
}



void QueueDestroy(Queue *pq) {
    assert(pq);
    QueueNode *cur = pq->head;
    while (cur) {
        QueueNode *next = cur->next;
        free(cur);
        cur = next;
    }
    pq->head = pq->tail = NULL;
}


void QueuePush(Queue *pq, QDataType x) {
    assert(pq);
    // 先对新节点赋值
    QueueNode *newNode = (QueueNode *) malloc(sizeof(QueueNode));
    newNode->data = x;
    newNode->next = NULL;
    if (pq->head == NULL) {
        pq->head = pq->tail = newNode;
    } else {
        pq->tail->next = newNode;
        pq->tail = newNode;
    }
}

void QueuePop(Queue *pq) {
    /*
     * 从队列 head处删除数据
     */
    assert(!QueueEmpty(pq));

    QueueNode *next = pq->head->next;
    free(pq->head);
    pq->head = next;
    if (pq->head == NULL) {
        pq->tail = NULL;
    }

}

bool QueueEmpty(Queue *pq) {
    assert(pq);
    return pq->head == NULL;
}

int QueueSize(Queue *pq) {
    assert(!QueueEmpty(pq));
    QueueNode *cur = pq->head;
    int n = 0;

    while (cur) {
        n++;
        cur = cur->next;
    }
    return n;
}


QDataType QueueFront(Queue *pq) {
    assert(!QueueEmpty(pq));
    return pq->head->data;
}

QDataType QueueBack(Queue *pq) {
    assert(!QueueEmpty(pq));
    return pq->tail->data;
}

MyStack *myStackCreate() {
    MyStack *myStack;
    myStack = (MyStack *) malloc(sizeof(MyStack));
    QueueInit(&myStack->q1);
    QueueInit(&myStack->q2);
    return myStack;
}

void myStackPush(MyStack *obj, int x) {
    if (!QueueEmpty(&obj->q1)) {
        QueuePush(&obj->q1, x);
    } else {
        QueuePush(&obj->q2, x);
    }
}

int myStackPop(MyStack *obj) {
    Queue *emptyQ = &obj->q1;
    Queue *nonemptyQ = &obj->q2;
    if (!QueueEmpty(&obj->q1)) {
        emptyQ = nonemptyQ;
        nonemptyQ = &obj->q1;
    }
    while (QueueSize(nonemptyQ) > 1) {
        QueuePush(emptyQ, QueueFront(nonemptyQ));
        QueuePop(nonemptyQ);
    }
    int top = QueueFront(nonemptyQ);
    QueuePop(nonemptyQ);
    return top;
}

int myStackTop(MyStack *obj) {
    if(!QueueEmpty(&obj->q1)){
        return QueueBack(&obj->q1);
    } else {
        return QueueBack(&obj->q2);
    }
}

bool myStackEmpty(MyStack *obj) {
    return QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2);
}

void myStackFree(MyStack *obj) {
    QueueDestroy(&obj->q1);
    QueueDestroy(&obj->q2);
    free(obj);
    obj = NULL;
}

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);

 * int param_2 = myStackPop(obj);

 * int param_3 = myStackTop(obj);

 * bool param_4 = myStackEmpty(obj);

 * myStackFree(obj);
*/