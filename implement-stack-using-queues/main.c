#include "include/stack_using_queues.h"

int main() {
    MyStack *st = myStackCreate();
    myStackPush(st, 1);
    myStackPush(st, 2);
    myStackPush(st, 5);
    myStackPop(st);
    printf("Hello, World!\n");
    return 0;
}
