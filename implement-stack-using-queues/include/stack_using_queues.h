//
// Created by IT-Desktop on 2022/8/22.
//

#ifndef IMPLEMENT_STACK_USING_QUEUES_STACK_USING_QUEUES_H
#define IMPLEMENT_STACK_USING_QUEUES_STACK_USING_QUEUES_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


typedef int QDataType;
typedef struct QueueNode {
    struct QueueNode *next;
    QDataType data;
}QueueNode;

typedef struct {
    QueueNode *head;
    QueueNode *tail;
//    size_t size;
}Queue;

typedef struct {
    Queue q1;
    Queue q2;
} MyStack;


MyStack* myStackCreate();

void myStackPush(MyStack* obj, int x);

int myStackPop(MyStack* obj);

int myStackTop(MyStack* obj);

bool myStackEmpty(MyStack* obj);

void myStackFree(MyStack* obj);

void QueueInit(Queue *pq);
void QueuePush(Queue *pq, QDataType x);
bool QueueEmpty(Queue *pq);
void QueuePop(Queue *pq);
int QueueSize(Queue *pq);
QDataType QueueFront(Queue *pq);
void QueueDestroy(Queue *pq);


#endif //IMPLEMENT_STACK_USING_QUEUES_STACK_USING_QUEUES_H
