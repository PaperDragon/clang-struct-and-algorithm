//
// Created by SuperNu1L on 2022/8/12.
//

#ifndef REVERSE_LINKED_LIST_REVERSE_LINKED_LIST_H
#define REVERSE_LINKED_LIST_REVERSE_LINKED_LIST_H
#include <stdio.h>
#include <stdlib.h>

struct ListNode* reverseList(struct ListNode* head);

void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);

struct ListNode {
    int val;
    struct ListNode *next;
};






#endif //REVERSE_LINKED_LIST_REVERSE_LINKED_LIST_H
