//
// Created by SuperNu1L on 2022/8/12.
//

#include "../include/reverse-linked-list.h"

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */



void SListPrint(struct ListNode *phead) {
    struct ListNode *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}

struct ListNode *BuytListNode(int x) {
    struct ListNode *newNode = (struct ListNode *) malloc(sizeof(int));
    newNode->val = x;
    newNode->next = NULL;
    return newNode;
}


void SListPushBack(struct ListNode **pphead, int x) {
    struct ListNode *newNode = BuytListNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        struct ListNode *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}

struct ListNode *reverseList(struct ListNode *head) {
    struct ListNode *after = head;
    if (head == NULL || head->next == NULL) {
        return head;
    } else if ((head->next)->next != NULL) {
        struct ListNode *cur = NULL;
        struct ListNode *before = NULL;
        int i = 1;
        while (after != NULL) {
            if (before != NULL) {
                cur->next = before;
                if (i == 1) {
                    before->next = NULL;
                    i--;
                }
            }
            before = cur;
            cur = after;
            after = after->next;
        }
        cur->next = before;
        head = cur;
    } else {
        head = head->next;
        head->next = after;
        after->next = NULL;
    }
    return head;
}

