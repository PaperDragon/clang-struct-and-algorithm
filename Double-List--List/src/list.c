//
// Created by SuperNu1L on 2022/8/18.
//
#include "../include/list.h"

ListNode *BuyListNode(LTDataType x);

ListNode *ListInit() {
    ListNode *phead = (ListNode *) malloc(sizeof(ListNode));
    phead->next = phead;
    phead->prev = phead;
    phead->data = 0;
    return phead;
}

void ListPushBack(ListNode *phead, LTDataType x) {
    ListNode *newNode = BuyListNode(x);
    assert(phead);
    ListNode *tail = phead->prev;
    tail->next = newNode;
    newNode->prev = tail;
    newNode->next = phead;
    phead->prev = newNode;
}

void ListPrint(ListNode *phead) {
    ListNode *head = phead;
    do {
        printf("%d->", head->data);
        head = head->next;
    } while (head != phead);
    printf("head\n");
}

void ListPopBack(ListNode *phead) {
    assert(phead->next != phead && phead);
    ListNode *thead = phead->prev;

    phead->prev->prev = phead;
    phead->prev->prev->next = phead;
    free(thead);

}

ListNode *ListFind(ListNode *phead, LTDataType x) {
    assert(phead);
    ListNode *cur = phead->next;
    while (cur != phead) {
        if (cur->data == x) {
            return cur;
        }
        cur = cur->next;
    }
    return NULL;
}

void ListInsert(ListNode *pos, LTDataType x) {
    assert(pos);
    ListNode *posPrev = pos->prev;
    ListNode *newNode;
    newNode = BuyListNode(x);

    posPrev->next = newNode;
    newNode->prev = pos->prev;
    newNode->next = pos;
    pos->prev = newNode;
}


ListNode *BuyListNode(LTDataType x) {
    ListNode *newNode = (ListNode *) malloc(sizeof(ListNode));
    newNode->data = x;
    return newNode;
}

void ListErase(ListNode *pos) {
    assert(pos);

    ListNode *posPrev = pos->prev;
    ListNode *posNext = pos->next;

    posPrev->next = posNext;
    posNext->prev = posPrev;
    free(pos);
    pos = NULL;
}


void ListDestroy(ListNode *phead) {
    assert(phead);
    ListNode *cur = phead->next;
    while (cur != phead) {
        ListNode *next = cur->next;
        free(cur);
        cur = next;
    }
    free(phead);
    phead = NULL;
}


