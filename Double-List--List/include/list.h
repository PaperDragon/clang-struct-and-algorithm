//
// Created by SuperNu1L on 2022/8/18.
//

#ifndef DOUBLE_LIST__LIST_LIST_H
#define DOUBLE_LIST__LIST_LIST_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


typedef int LTDataType;

typedef struct ListNode {
    LTDataType data;
    struct ListNode *next;
    struct ListNode *prev;
} ListNode;


ListNode *ListInit();

void ListPushBack(ListNode *phead, LTDataType x);

void ListPopBack(ListNode *phead);

void ListPrint(ListNode *phead);

ListNode *ListFind(ListNode *phead, LTDataType x);

void ListInsert(ListNode *pos, LTDataType x);

void ListErase(ListNode *pos);


ListNode *BuyListNode(LTDataType x);



#endif //DOUBLE_LIST__LIST_LIST_H