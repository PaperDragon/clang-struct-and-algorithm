//
// Created by SuperNu1L on 2022/8/6.
//

#include "include/SList.h"

int main(void) {
    SListNode *plist = NULL;
    for (int i = 0; i < 8 ; i++) {
        SListPushBack(&plist, i);
        SListPushBack(&plist, 7);
    }
    SListPrint(plist);
    SListPushFront(&plist, 50);
    SListPrint(plist);
    SListPopBack(&plist);
    SListPopBack(&plist);
    SListPrint(plist);
    SListPopFront(&plist);
    SListPrint(plist);
    SListNode *res = SListFind(plist, 0);
    printf("Data %d nextNode address is %p\n", res->data, res->next);
    SListNode *res1 = plist;
    int i = 1;
    while (res1->next != NULL) {
        res1 = SListFind(res1->next, 7);
        printf("Sum %d Node is : %p->%d\n", i, res1, res1->data);
        i++;
    }
    SListPrint(plist);
    return 0;
}