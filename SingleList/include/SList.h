//
// Created by SuperNu1L on 2022/8/6.
//

#ifndef SINGLELIST_SLIST_H
#define SINGLELIST_SLIST_H

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

typedef int SLDataType;

typedef struct SListNode {
    SLDataType data;
    struct SListNode *next;
} SListNode;

SListNode *BuySListNode(SLDataType x);

void SListPrint(SListNode *phead);

void SListPushBack(SListNode **pphead, SLDataType x);

void SListPopBack(SListNode **pphead);

void SListPushFront(SListNode **pphead, SLDataType x);

void SListPopFront(SListNode **pphead);

SListNode *SListFind(SListNode *phead, SLDataType x);

// 在pos之前插入一个节点
void SListInsert(SListNode **pphead, SListNode *pos, SLDataType x);

void SListInsertAfter(SListNode * pos, SLDataType x);

void SListErase(SListNode **pphead, SListNode *pos);

void SListDestory(SListNode **phead);

#endif //SINGLELIST_SLIST_H
