//
// Created by SuperNu1L on 2022/8/6.
//

#include <assert.h>
#include "../include/SList.h"

void SListPrint(SListNode *phead) {
    SListNode *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->data);
        cur = cur->next;
    }
    printf("NULL\n");
}


void SListPushBack(SListNode **pphead, SLDataType x) {
    SListNode *newNode = BuySListNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        SListNode *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}


void SListPushFront(SListNode **pphead, SLDataType x) {
    SListNode *newNode = BuySListNode(x);

    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        newNode->next = *pphead;
        *pphead = newNode;
    }
}

SListNode *BuySListNode(SLDataType x) {
    SListNode *newNode = (SListNode *) malloc(sizeof(SLDataType));
    newNode->data = x;
    newNode->next = NULL;
    return newNode;
}


void SListPopBack(SListNode **pphead) {
    assert(*pphead != NULL);
    if ((*pphead)->next == NULL) {
        free(*pphead);
        *pphead = NULL;
    } else {
        SListNode *tail = *pphead;
        SListNode *preTail = NULL;
        while (tail->next) {
            preTail = tail;
            tail = tail->next;
        }
        preTail->next = NULL;
        free(tail);
        tail = NULL;
    }
}

void SListPopFront(SListNode **pphead) {
    SListNode *tmpNode = *pphead;
    assert(*pphead != NULL);
    *pphead = (*pphead)->next;
    free(tmpNode);
}

SListNode *SListFind(SListNode *phead, SLDataType x) {
    SListNode *cur = phead;
    while (cur != NULL) {
        if (cur->data == x) {
            return cur;
        }
        cur = cur->next;
    }
    return NULL;
}


void SListInsert(SListNode **pphead, SListNode *pos, SLDataType x) {
    assert(pos);
    assert(pphead);

    // 头插
    if (pos == *pphead) {
        SListPushFront(pphead, x);
    } else {
        SListNode *prev = *pphead;
        while (prev->next != pos) {
            prev = prev->next;
        }

        SListNode *newnode = BuySListNode(x);
        prev->next = newnode;
        newnode->next = pos;
    }
}


// 删除pos位置的值
void SListErase(SListNode **pphead, SListNode *pos) {
    assert(pphead);
    assert(pos);

    if (*pphead == pos) {
        SListPopFront(pphead);
    } else {
        SListNode *prev = *pphead;
        while (prev->next != pos) {
            prev = prev->next;
        }

        prev->next = pos->next;

        free(pos);
        pos = NULL;
    }
}

// 单链表在pos位置之后插入x
void SListInsertAfter(SListNode *pos, SLDataType x) {
    assert(pos);

    /*SLTNode* newnode = BuySListNode(x);
    newnode->next = pos->next;
    pos->next = newnode;*/

    // 不在乎链接顺序
    SListNode *newnode = BuySListNode(x);
    SListNode *next = pos->next;
    // pos newnode next
    pos->next = newnode;
    newnode->next = next;
}
