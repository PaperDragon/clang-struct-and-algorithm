//
// Created by IT-Desktop on 2022/8/3.
//
#include "include/removeElement.h"


int main(void) {
    int seqTeam[] = {345,345,123,345,657,79,345};
    elementPrint(seqTeam,sizeof(seqTeam) / sizeof(int));
    int size = removeElementS1(seqTeam, sizeof(seqTeam) / sizeof(int), 345);
    printf("Size=%d\n", size);
    elementPrint(seqTeam,size);
    return 0;
}