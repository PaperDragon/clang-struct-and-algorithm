#include "../include/removeElement.h"

int removeElementS1(int *nums, int numsSize, int val) {
    int tmpHead = 0;
    for (int i = 0; i < numsSize; i++) {
        if(nums[i] != val){
            nums[tmpHead] = nums[i];
            tmpHead++;
        }
    }
    nums[tmpHead] = '\0';
    return tmpHead;
}

void elementPrint(int *nums, int numsSize) {
    int size = 0;
    while (size < numsSize) {
        printf("%d ",nums[size]);
        size ++;
    }
    printf("\n");
}