#include <stdio.h>
#include <malloc.h>

int contralArray(int **numsa, int size);

void SeqListPrint(int *ps, int size);


int contralArray(int **numsa, int size) {
    int *b = (int *)malloc(sizeof(int )* 10);
    for (int i = 10; i < 20; ++i) {
        b[i-10] = i;
    }
    *numsa = b;
    return 10;
}

void SeqListPrint(int *ps, int size) {
    for (int i = 0; i < size; ++i) {
        printf("nums[%d]=%d  ", i, ps[i]);
    }
    printf("\n");
}


int main() {
    int *nums = (int *)malloc(sizeof(int )* 10);
    for (int i = 0; i < 10; ++i) {
        nums[i] = i;
    }
    SeqListPrint(nums, 10);
    int size = contralArray(&nums, 10);


    SeqListPrint(nums, size);
    free(nums);
    return 0;
}
