//
// Created by IT-Desktop on 2022/8/16.
//

#ifndef COPY_LIST_WITH_RANDOM_POINTER_COPY_LIST_WITH_RANDOM_POINTER_H
#define COPY_LIST_WITH_RANDOM_POINTER_COPY_LIST_WITH_RANDOM_POINTER_H
#include <stdio.h>
#include <stdlib.h>

struct Node {
    int val;
    struct Node *next;
    struct Node *random;
};

struct Node* copyRandomList(struct Node* head);


void SListPrint(struct Node *phead);
void SListPushBack(struct Node **pphead, int x);
struct Node *BuytNode(int x);







#endif //COPY_LIST_WITH_RANDOM_POINTER_COPY_LIST_WITH_RANDOM_POINTER_H
