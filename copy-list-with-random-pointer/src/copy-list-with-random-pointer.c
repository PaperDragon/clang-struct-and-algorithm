//
// Created by IT-Desktop on 2022/8/16.
//
#include "../include/copy-list-with-random-pointer.h"

/**
 * Definition for a Node.
 * struct Node {
 *     int val;
 *     struct Node *next;
 *     struct Node *random;
 * };
 */

struct Node *BuytNode(int x) {
    struct Node *newNode = (struct Node *) malloc(sizeof(struct Node));
    newNode->val = x;
    newNode->next = NULL;
    newNode->random = NULL;
    return newNode;
}

void SListPushBack(struct Node **pphead, int x) {
    struct Node *newNode = BuytNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        struct Node *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}

void SListPrint(struct Node *phead) {
    struct Node *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}


struct Node *copyRandomList(struct Node *head) {
    struct Node *cur = head;
    while (cur) {
        struct Node *copy = (struct Node *) malloc(sizeof(struct Node));
        copy->val = cur->val;
        copy->next = cur->next;
        cur->next = copy;

        cur = copy->next;
    }

    cur = head;
    while (cur) {
        struct Node *copy = cur->next;
        if (cur->random == NULL) {
            copy->random = NULL;
        } else {
            copy->random = cur->random->next;
        }
        cur = copy->next;
    }

    struct Node *copyHead = NULL;
    struct Node *copyTail = NULL;

    cur = head;
    while (cur) {
        struct Node *copy = cur->next;
        struct Node *next = copy->next;

        if (copyTail == NULL) {
            copyHead = copy;
            copyTail = copy;
        } else {
            copyTail->next = copy;
            copyTail = copy;
        }
        cur->next = next;

        cur = next;
    }

    return copyHead;
}

