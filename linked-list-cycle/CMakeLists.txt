cmake_minimum_required(VERSION 3.23)
project(linked_list_cycle C)

set(CMAKE_C_STANDARD 99)

add_executable(linked_list_cycle main.c include/linked-list-cycle.h src/linked-list-cycle.c)
