#include "include/linked-list-cycle.h"

int main() {
    struct ListNode * a = NULL;
    for (int i = 0; i < 11; ++i) {
        SListPushBack(&a,i);
    }
    SListPrint(a);
    struct ListNode * b = a;
    while (b->next) {
        b = b->next;
    }
//    b->next = a->next->next;
    bool c = hasCycle(a);
    c == 1 ? printf("True") : printf("False");
    return 0;
}