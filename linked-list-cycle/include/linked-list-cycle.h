//
// Created by IT-Desktop on 2022/8/15.
//

#ifndef LINKED_LIST_CYCLE_LINKED_LIST_CYCLE_H
#define LINKED_LIST_CYCLE_LINKED_LIST_CYCLE_H
#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

bool hasCycle(struct ListNode *head) ;

void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);
struct ListNode *BuytListNode(int x);



#endif //LINKED_LIST_CYCLE_LINKED_LIST_CYCLE_H
