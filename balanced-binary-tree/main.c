#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};


int main() {
    printf("Hello, World!\n");
    return 0;
}



int maxDepth(struct TreeNode *root);

bool isBalanced(struct TreeNode* root);

int maxDepth(struct TreeNode *root) {
    if(root == NULL){
        return 0;
    }
    int leftDepth = maxDepth(root->left);
    int rightDepth = maxDepth(root->right);
    return leftDepth> rightDepth ? leftDepth+1 : rightDepth+1;
}

bool isBalanced(struct TreeNode *root) {
    if(root == NULL)
        return true;
    int leftDepth = maxDepth(root->left);
    int rightDepth = maxDepth(root->right);
    return abs(leftDepth - rightDepth) < 2 && isBalanced(root->left) && isBalanced(root->right);

}

