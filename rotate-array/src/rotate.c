
#include "../include/rotate.h"

void rotate(int *nums, int numsSize, int k) {

    int *ret = (int *) malloc(sizeof(int) * numsSize);
    memset(ret, 0, sizeof(int) * numsSize);//开辟一个新数组
    int i = 0;
    int j = k % numsSize;//对旋转次数取余
    if (numsSize > 1 && j != 0)//判断元素个数是否为一，若是一则不必处理
    {
        for (i = 0; i < j; i++) {
            ret[i] = nums[numsSize - j + i];//将nums数组中最后k个元素拷贝到ret数组中
        }
        for (i = 0; i < numsSize - j; i++) {
            ret[j + i] = nums[i];//把nums数组中前面的元素拷贝到ret数组中
        }
        for (i = 0; i < numsSize; i++) {
            nums[i] = ret[i];//将ret数组拷贝回原数组
        }
    }
}


void print(int *nums, int numsSize) {
    int size = 0;
    while (size < numsSize) {
        printf("%d ", nums[size]);
        size++;
    }
    printf("\n");
}
