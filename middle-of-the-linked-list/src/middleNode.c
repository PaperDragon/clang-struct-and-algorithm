//
// Created by SuperNu1L on 2022/8/12.
//
#include "../include/middleNode.h"


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode *middleNode(struct ListNode *head) {
    struct ListNode *slow = head;
    struct ListNode *fast = head;
    while ( fast && fast->next ) {
        slow = slow->next;
        fast= fast->next->next;
    }
    return slow;
}

struct ListNode *BuytListNode(int x) {
    struct ListNode *newNode = (struct ListNode *) malloc(sizeof(int));
    newNode->val = x;
    newNode->next = NULL;
    return newNode;
}




void SListPushBack(struct ListNode **pphead, int x) {
    struct ListNode *newNode = BuytListNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        struct ListNode *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}

void SListPrint(struct ListNode *phead) {
    struct ListNode *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}
