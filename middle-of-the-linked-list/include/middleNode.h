#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};



struct ListNode* middleNode(struct ListNode* head);
void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);