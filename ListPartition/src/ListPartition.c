//
// Created by SuperNu1L on 2022/8/14.
//

#include "../include/ListPartition.h"

struct ListNode *BuytListNode(int x) {
    struct ListNode *newNode = (struct ListNode *) malloc(sizeof(struct ListNode));
    newNode->val = x;
    newNode->next = NULL;
    return newNode;
}

void SListPushBack(struct ListNode **pphead, int x) {
    struct ListNode *newNode = BuytListNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        struct ListNode *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}

void SListPrint(struct ListNode *phead) {
    struct ListNode *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */




struct ListNode *partition(struct ListNode *head, int x) {
    struct ListNode *lessHead, *lessTail, *greaterHead, *greaterTail;
    // 开一个哨兵位
    lessHead = lessTail = (struct ListNode *) malloc(sizeof(struct ListNode));
    lessTail->next = NULL;
    greaterHead = greaterTail = (struct ListNode *) malloc(sizeof(struct ListNode));
    greaterTail->next = NULL;

    struct ListNode *cur;
    cur = head;
    while (cur) {
        if (cur->val < x) {
            lessTail->next = cur;
            lessTail = cur;
        } else {
            greaterTail->next = cur;
            greaterTail = cur;
        }
        cur = cur->next;
    }
    lessTail->next = greaterHead->next;
    greaterTail->next = NULL;
    struct ListNode* newHead = lessHead->next;
    free(lessHead);
    free(greaterHead);
    return newHead;
}
