//
// Created by SuperNu1L on 2022/8/14.
//

#ifndef LISTPARTITION_LISTPARTITION_H
#define LISTPARTITION_LISTPARTITION_H
#include <stdio.h>
#include <malloc.h>


struct ListNode {
    int val;
    struct ListNode *next;
};


void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);
struct ListNode *BuytListNode(int x);

struct ListNode* partition(struct ListNode* head, int x);


#endif //LISTPARTITION_LISTPARTITION_H
