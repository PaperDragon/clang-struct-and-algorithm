//
// Created by IT-Desktop on 2022/8/24.
//

#ifndef BINARYTREE_BINTREE_H
#define BINARYTREE_BINTREE_H
#include <stdio.h>
#include <stdlib.h>


typedef char BTDataType;

typedef struct BinaryTreeNode {
    struct BinaryTreeNode* left;
    struct BinaryTreeNode* right;
    BTDataType data;
} BTNode;

void PrevOrder(BTNode *root);
void InOrder(BTNode *root);
void PostOrder(BTNode *root);



#endif //BINARYTREE_BINTREE_H
