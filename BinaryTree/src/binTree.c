//
// Created by IT-Desktop on 2022/8/24.
//
#include "../include/binTree.h"


void PrevOrder(BTNode *root) {
    if (root == NULL) {
        return;
    }
//    printf("%c->", root->data);
    PrevOrder(root->left);
    PrevOrder(root->right);
}


void InOrder(BTNode *root) {
    if (root == NULL) {
        return;
    }
    InOrder(root->left);
    printf("%c->", root->data);
    InOrder(root->right);
}

void PostOrder(BTNode *root) {
    if (root == NULL) {
        return;
    }
    PostOrder(root->left);
    PostOrder(root->right);
    printf("%c->", root->data);
}

void Order(BTNode *root) {
    if (root == NULL) {
        return;
    }
    Order(root->left);
    Order(root->right);
}


