//
// Created by IT-Desktop on 2022/8/23.
//

#ifndef IMPLEMENT_QUEUE_USING_StackACKS_IMPLEMENT_QUEUE_USING_StackACKS_H
#define IMPLEMENT_QUEUE_USING_StackACKS_IMPLEMENT_QUEUE_USING_StackACKS_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "assert.h"

typedef int StackDataType;

typedef struct Stack {
    StackDataType *a;
    int top;
    int capacity;
} Stack;

void StackInit(Stack *ps);
void StackDestroy(Stack *ps);

void StackPush(Stack *ps, StackDataType x);
void StackPop(Stack *ps);

StackDataType StackTop(Stack *ps);

bool StackEmpty(Stack* ps);


typedef struct {
    Stack pushStack;
    Stack popStack;
} MyQueue;
MyQueue *myQueueCreate();
void myQueuePush(MyQueue *obj, int x);
int myQueuePeek(MyQueue *obj);
bool myQueueEmpty(MyQueue *obj);
int myQueuePop(MyQueue *obj);

void myQueueFree(MyQueue *obj);








#endif //IMPLEMENT_QUEUE_USING_StackACKS_IMPLEMENT_QUEUE_USING_StackACKS_H
