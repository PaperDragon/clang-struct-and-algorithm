#include "include/implement-queue-using-stacks.h"

int main() {
    MyQueue * q = myQueueCreate();
    myQueuePush(q,1);
    myQueuePush(q,2);
    myQueuePush(q,5);
    myQueuePush(q,8);
    myQueuePush(q,9);

    int front  = myQueuePeek(q);
    printf("%d\n",front);

    int pop = myQueuePop(q);
    printf("pop=%d\n",pop);

    bool emp = myQueueEmpty(q);
    printf("%d\n",emp);


    myQueueFree(q);
    return 0;
}
