//
// Created by IT-Desktop on 2022/8/23.
//
#include "../include/implement-queue-using-stacks.h"

void StackInit(Stack *ps) {
    /*
     * 初始化时
     * ps->top = 0 表示指向栈顶数据的下一个
     * ps->top = -1 表示指向栈顶数据
     */
    assert(ps);
    ps->a = NULL;
    ps->top = 0;
    ps->capacity = 0;
}

void StackDestroy(Stack *ps) {
    assert(ps);
    free(ps->a);
    ps->a = NULL;
    ps->capacity = ps->top = 0;
}

void StackPush(Stack *ps, StackDataType x) {
    assert(ps);
    if (ps->top == ps->capacity) {
        int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
        StackDataType *tmp = realloc(ps->a, sizeof(StackDataType) * newCapacity);
        if (tmp == NULL) {
            printf("re alloc fail!\n");
            exit(-1);
        }
        ps->a = tmp;
        ps->capacity = newCapacity;
    }
    ps->a[ps->top] = x;
    ps->top++;
}

void StackPop(Stack *ps) {
    assert(ps);
    ps->top--;
}

StackDataType StackTop(Stack *ps) {
    assert(ps);
    assert(!StackEmpty(ps));
    return ps->a[ps->top - 1];
}

bool StackEmpty(Stack *ps) {
    assert(ps);
    return ps->top == 0;
}


MyQueue *myQueueCreate() {
    MyQueue *q = (MyQueue *) malloc(sizeof(MyQueue));
    StackInit(&q->pushStack);
    StackInit(&q->popStack);
    return q;
}

void myQueuePush(MyQueue *obj, int x) {
    StackPush(&obj->pushStack, x);
}


int myQueuePop(MyQueue *obj) {
    if (StackEmpty(&obj->popStack)) {
        while (!StackEmpty(&obj->pushStack)) {
            StackPush(&obj->popStack, StackTop(&obj->pushStack));
            StackPop(&obj->pushStack);
        }
    }
    int front = StackTop(&obj->popStack);
    StackPop(&obj->popStack);
    return front;
}


int myQueuePeek(MyQueue *obj) {
    if (StackEmpty(&obj->popStack)) {
        while (!StackEmpty(&obj->pushStack)) {
            StackPush(&obj->popStack, StackTop(&obj->pushStack));
            StackPop(&obj->pushStack);
        }
    }
    return StackTop(&obj->popStack);
}


bool myQueueEmpty(MyQueue *obj) {
    return StackEmpty(&obj->popStack) && StackEmpty(&obj->pushStack);
}


void myQueueFree(MyQueue *obj) {
    StackDestroy(&obj->pushStack);
    StackDestroy(&obj->popStack);
    free(obj);
    obj = NULL;

}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/