#include <stdio.h>

int main() {


    printf("Hello, World!\n");
    return 0;
}

struct TreeNode {
    struct TreeNode *left;
    struct TreeNode *right;
    int data;
};

int maxDepth(struct TreeNode *root);

int maxDepth(struct TreeNode *root) {
    if(root == NULL){
        return 0;
    }
    int leftDepth = maxDepth(root->left);
    int rightDepth = maxDepth(root->right);
    return leftDepth> rightDepth ? leftDepth+1 : rightDepth+1;
}
