//
// Created by SuperNu1L on 2022/8/20.
//

#ifndef VALID_PARENTHESES_VALID_PARENTHESES_H
#define VALID_PARENTHESES_VALID_PARENTHESES_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "assert.h"
bool isValid(char * s);

typedef char STDataType;

typedef struct Stack {
    STDataType *a;
    int top;
    int capacity;
} Stack;

void StackPush(Stack* ps, STDataType x);
void StackInit(Stack* ps);
void StackDestory(Stack* ps);
void StackPop(Stack* ps);
bool StackEmpty(Stack* ps);
STDataType StackTop(Stack* ps);

#endif //VALID_PARENTHESES_VALID_PARENTHESES_H
