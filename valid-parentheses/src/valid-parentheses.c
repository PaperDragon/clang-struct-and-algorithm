
#include "../include/valid-parentheses.h"


void StackInit(Stack *ps) {
    assert(ps);

    ps->a = NULL;
    ps->capacity = 0;
    ps->top = 0;
}


void StackDestory(Stack *ps) {
    assert(ps);

    free(ps->a);
    ps->capacity = 0;
    ps->top = 0;
}


void StackPush(Stack *ps, STDataType x) {
    assert(ps);
    if (ps->top == ps->capacity) {
        int newCapcity = ps->capacity == 0 ? 4 : ps->capacity * 2;
        ps->a = (STDataType *) realloc(ps->a, sizeof(int) * newCapcity);
        if (ps->a == NULL) {
            printf("ralloc error");
            exit(-1);
        }
        ps->capacity = newCapcity;
    }

    ps->a[ps->top] = x;
    ps->top++;
}


void StackPop(Stack *ps) {
    assert(ps);
    assert(ps->top > 0);

    ps->top--;
}


bool StackEmpty(Stack *ps) {
    assert(ps);
    return ps->top == 0;
}


STDataType StackTop(Stack *ps) {
    assert(ps);
    assert(ps->top > 0);

    return ps->a[ps->top - 1];
}

bool isValid(char *s) {
    Stack stack01;
    StackInit(&stack01);
    for (int i = 0; s[i] != '\0'; i++) {
        //空栈则先压栈
        if (stack01.top == 0) {
            StackPush(&stack01, s[i]);
        } else if (s[i] == ')' && stack01.a[stack01.top - 1] == '(') { //可以匹配则出栈
            StackPop(&stack01);
        } else if (s[i] == '}' && stack01.a[stack01.top - 1] == '{') {
            StackPop(&stack01);
        } else if (s[i] == ']' && stack01.a[stack01.top - 1] == '[') {
            StackPop(&stack01);
        } else {
            StackPush(&stack01, s[i]);
        } //无法匹配，入栈
    }
    return stack01.top == 0;
}