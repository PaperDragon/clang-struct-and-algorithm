//
// Created by SuperNu1L on 2022/8/12.
//

#ifndef MERGE_TWO_SORTED_LISTS_MERGE_TWO_SORTED_LISTS_H
#define MERGE_TWO_SORTED_LISTS_MERGE_TWO_SORTED_LISTS_H

#include <stdio.h>

struct ListNode {
    int val;
    struct ListNode *next;
};



struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2);

void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);
struct ListNode *BuytListNode(int x);




#endif //MERGE_TWO_SORTED_LISTS_MERGE_TWO_SORTED_LISTS_H
