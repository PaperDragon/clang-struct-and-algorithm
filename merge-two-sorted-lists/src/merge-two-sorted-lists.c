//
// Created by SuperNu1L on 2022/8/12.
//
#include "../include/merge-two-sorted-lists.h"


struct ListNode *BuytListNode(int x) {
    struct ListNode *newNode = (struct ListNode *) malloc(sizeof(int));
    newNode->val = x;
    newNode->next = NULL;
    return newNode;
}

void SListPushBack(struct ListNode **pphead, int x) {
    struct ListNode *newNode = BuytListNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        struct ListNode *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}

void SListPrint(struct ListNode *phead) {
    struct ListNode *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}


struct ListNode *mergeTwoLists(struct ListNode *list1, struct ListNode *list2) {
    if (list1 == NULL) return list2;
    if (list2 == NULL) return list1;
    if (list1->next == NULL  && list1->val < list2->val) {
        list1->next = list2;
        return list1;
    } else if(list2->next == NULL && list1->val > list2->val){
        list2->next = list1;
        return list2;
    }
    struct ListNode *head= NULL;
    struct ListNode *tail = NULL;
    while (list1 && list2) {
        if(list1->val < list2->val) {
            if(head == NULL){
                head = list1;
                tail = list1;
            } else {
                tail->next = list1;
                tail = list1;
            }
            list1 = list1->next;
        } else {
            if(head == NULL ){
                head = list2;
                tail = list2;
            } else{
                tail->next = list2;
                tail = list2;
            }
            list2 = list2->next;
        }
    }
    if(list1) {
        tail->next = list1;
    }
    if (list2) {
        tail->next = list2;
    }
    return head;
}

