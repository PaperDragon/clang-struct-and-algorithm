//
// Created by SuperNu1L on 2022/8/12.
//

#ifndef FINDKTHTOTAIL_FINDKTHTOTAIL_H
#define FINDKTHTOTAIL_FINDKTHTOTAIL_H


#include <stdio.h>


/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

/**
 *
 * @param pListHead ListNode类
 * @param k int整型
 * @return ListNode类
 */
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k ) ;

struct ListNode {
	int val;
	struct ListNode *next;
};

void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);
struct ListNode *BuytListNode(int x);

#endif //FINDKTHTOTAIL_FINDKTHTOTAIL_H
