//
// Created by SuperNu1L on 2022/8/12.
//

#include "../include/FindKthToTail.h"

/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

/**
 *
 * @param pListHead ListNode类
 * @param k int整型
 * @return ListNode类
 */


struct ListNode* FindKthToTail(struct ListNode* pListHead, int k ) {
    // write code here
    if(pListHead == NULL || k == 0) {
        return NULL;
    }
    struct ListNode *low,*fast;
    low = fast = pListHead;
    while (k--){
        if(fast == NULL) {
            return NULL;
        }
        fast = fast->next;
    }
    while (fast) {
        fast = fast->next;
        low = low->next;
    }
    return low;
}



struct ListNode *BuytListNode(int x) {
    struct ListNode *newNode = (struct ListNode *) malloc(sizeof(int));
    newNode->val = x;
    newNode->next = NULL;
    return newNode;
}

void SListPushBack(struct ListNode **pphead, int x) {
    struct ListNode *newNode = BuytListNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        struct ListNode *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}

void SListPrint(struct ListNode *phead) {
    struct ListNode *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}
