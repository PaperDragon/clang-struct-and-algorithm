#include "include/intersection-of-two-linked-lists.h"


int main() {
    struct ListNode *a = NULL;
    struct ListNode *b = NULL;
    SListPrint(a);
    SListPushBack(&a, 1);
    SListPushBack(&a, 9);
    SListPushBack(&a, 1);
    SListPushBack(&b, 3);
    SListPrint(a);
    SListPrint(b);

    struct ListNode *newNode = BuytListNode(2);
    struct ListNode *c = NULL;
    struct ListNode *d = NULL;
    c = a;
    d = b;
    while (c->next) {
        c = c->next;
    }
    while (d->next) {
        d = d->next;
    }
    c->next = newNode;
    d->next = newNode;
    newNode->next = BuytListNode(4);

    SListPrint(a);
    SListPrint(b);

    struct ListNode *e = getIntersectionNode(a, b);
    SListPrint(e);
}
