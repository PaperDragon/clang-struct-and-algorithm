//
// Created by SuperNu1L on 2022/8/14.
//

#ifndef INTERSECTION_OF_TWO_LINKED_LISTS_INTERSECTION_OF_TWO_LINKED_LISTS_H
#define INTERSECTION_OF_TWO_LINKED_LISTS_INTERSECTION_OF_TWO_LINKED_LISTS_H
#include <stdio.h>
#include <stdlib.h>


struct ListNode {
    int val;
    struct ListNode *next;
};



struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB);

void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);
struct ListNode *BuytListNode(int x);


#endif //INTERSECTION_OF_TWO_LINKED_LISTS_INTERSECTION_OF_TWO_LINKED_LISTS_H
