//
// Created by SuperNu1L on 2022/8/14.
//
#include "../include/intersection-of-two-linked-lists.h"


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

struct ListNode *BuytListNode(int x) {
    struct ListNode *newNode = (struct ListNode *) malloc(sizeof(struct ListNode));
    newNode->val = x;
    newNode->next = NULL;
    return newNode;
}

void SListPushBack(struct ListNode **pphead, int x) {
    struct ListNode *newNode = BuytListNode(x);
    if (*pphead == NULL) {
        *pphead = newNode;
    } else {
        struct ListNode *tail = *pphead;
        while (tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = newNode;
    }
}

void SListPrint(struct ListNode *phead) {
    struct ListNode *cur = phead;
    while (cur != NULL) {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}

struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {
    struct ListNode *tailA = headA;
    struct ListNode *tailB = headB;
    int lenA = 1;
    int lenB = 1;


    while (tailA->next) {
        tailA = tailA->next;
        lenA++;
    }
    while (tailB->next) {
        tailB = tailB->next;
        lenB++;
    }

    if (tailA != tailB) {
        return NULL;
    }
    int gap = abs(lenB - lenA);
    struct ListNode *longList = headA;
    struct ListNode *shortList = headB;
    if (lenA < lenB) {
        shortList = headA;
        longList = headB;
    }
    while (gap--){
        longList = longList->next;
    }

    while (longList != shortList) {
        longList = longList->next;
        shortList = shortList->next;
    }
    return longList;
}
