//
// Created by IT-Desktop on 2022/8/23.
//

#ifndef DESIGN_CIRCULAR_QUEUE_DESIGN_CIRCULAR_QUEUE_H
#define DESIGN_CIRCULAR_QUEUE_DESIGN_CIRCULAR_QUEUE_H
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct {
    int *a;
    int front;
    int tail;
    int k;
} MyCircularQueue;

bool myCircularQueueIsFull(MyCircularQueue *obj);
bool myCircularQueueIsEmpty(MyCircularQueue *obj);


#endif //DESIGN_CIRCULAR_QUEUE_DESIGN_CIRCULAR_QUEUE_H
