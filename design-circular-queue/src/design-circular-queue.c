//
// Created by IT-Desktop on 2022/8/23.
//
#include "../include/design-circular-queue.h"


MyCircularQueue *myCircularQueueCreate(int k) {
    MyCircularQueue *q = (MyCircularQueue *) malloc(sizeof(MyCircularQueue));
    q->a = (int *) malloc(sizeof(int) * (k + 1));
    q->front = 0;
    q->tail = 0;
    q->k = k;
    return q;
}

bool myCircularQueueEnQueue(MyCircularQueue *obj, int value) {
    if(myCircularQueueIsFull(obj)){
        return false;
    }
    obj->a[obj->tail] = value;
    ++obj->tail;
    obj->tail %= (obj->k+1);

    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue *obj) {
    if(myCircularQueueIsEmpty(obj)) {
        return false;
    }
    ++obj->front;
    obj->front %= (obj->k+1);

    return true;
}

int myCircularQueueFront(MyCircularQueue *obj) {
    if(myCircularQueueIsEmpty(obj)) {
        return -1;
    }
    return obj->a[obj->front];
}

int myCircularQueueRear(MyCircularQueue *obj) {
    if(myCircularQueueIsEmpty(obj)) {
        return -1;
    }

    int i = (obj->tail +obj->k) %(obj->k +1);
    return obj->a[i];
}



bool myCircularQueueIsEmpty(MyCircularQueue *obj) {
    return obj->front == obj->tail;
}


bool myCircularQueueIsFull(MyCircularQueue *obj) {
    return (obj->tail + 1) % (obj->k + 1) == obj->front;
}

void myCircularQueueFree(MyCircularQueue *obj) {
    free(obj->a);
    free(obj);
    obj=NULL;
}

/**
 * Your MyCircularQueue struct will be instantiated and called as such:
 * MyCircularQueue* obj = myCircularQueueCreate(k);
 * bool param_1 = myCircularQueueEnQueue(obj, value);

 * bool param_2 = myCircularQueueDeQueue(obj);

 * int param_3 = myCircularQueueFront(obj);

 * int param_4 = myCircularQueueRear(obj);

 * bool param_5 = myCircularQueueIsEmpty(obj);

 * bool param_6 = myCircularQueueIsFull(obj);

 * myCircularQueueFree(obj);
*/