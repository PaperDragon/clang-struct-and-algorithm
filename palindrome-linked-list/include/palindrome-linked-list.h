//
// Created by SuperNu1L on 2022/8/14.
//

#ifndef PALINDROME_LINKED_LIST_PALINDROME_LINKED_LIST_H
#define PALINDROME_LINKED_LIST_PALINDROME_LINKED_LIST_H

#include <stdio.h>
#include <stdbool.h>


void SListPrint(struct ListNode *phead);
void SListPushBack(struct ListNode **pphead, int x);
struct ListNode *BuytListNode(int x);

struct ListNode *middleNode(struct ListNode *head);
struct ListNode *reverseList(struct ListNode *head);

struct ListNode {
     int val;
     struct ListNode *next;
 };

bool isPalindrome(struct ListNode* head);



#endif //PALINDROME_LINKED_LIST_PALINDROME_LINKED_LIST_H
