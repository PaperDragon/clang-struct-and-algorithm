#include <stdio.h>
#include <malloc.h>


typedef struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
} TreeNode;




/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int *preorderTraversal(struct TreeNode *root, int *returnSize);

int TreeSize(struct TreeNode *root) {
    return root == NULL ? 0 : TreeSize(root->left) + TreeSize(root->right) + 1;
}

void _prevOrder(struct TreeNode *root, int *a, int *i){
    if(root == NULL){
        return;
    }
    a[*i] = root->val;
    ++*i;
    _prevOrder(root->left, a, i);
    _prevOrder(root->right, a, i);
}


int *preorderTraversal(struct TreeNode *root, int *returnSize) {
    if (root == NULL) {
        *returnSize = 0;
        return NULL;
    }
    int size = TreeSize(root);
    int *a = (int *) malloc(size * sizeof(int));
    int i = 0;
    _prevOrder(root, a, &i);

    *returnSize = size;
    return a;
}

int main() {
    TreeNode *A = (TreeNode *) malloc(sizeof(TreeNode));
    A->val = 2;
    A->left = NULL;
    A->right = NULL;

    TreeNode *B = (TreeNode *) malloc(sizeof(TreeNode));
    B->val = 3;
    B->left = NULL;
    B->right = NULL;

    TreeNode *C = (TreeNode *) malloc(sizeof(TreeNode));
    C->val = 4;
    C->left = NULL;
    C->right = NULL;

    TreeNode *D = (TreeNode *) malloc(sizeof(TreeNode));
    D->val = 5;
    D->left = NULL;
    D->right = NULL;

    TreeNode *E = (TreeNode *) malloc(sizeof(TreeNode));
    E->val = 6;
    E->left = NULL;
    E->right = NULL;

    A->left = B;
    A->right = C;
    B->left = D;
    B->right = E;

    int *c = NULL;
    c = preorderTraversal(A, c);
    printf("d = %d", *c);

    return 0;
}