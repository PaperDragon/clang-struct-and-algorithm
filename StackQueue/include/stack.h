//
// Created by SuperNu1L on 2022/8/20.
//

#ifndef STACKQUEUE_STACK_H
#define STACKQUEUE_STACK_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "assert.h"

typedef int STDataType;

typedef struct Stack {
    STDataType *a;
    int top;
    int capacity;
} ST;

void StackInit(ST *ps);
void StackDesroy(ST *ps);

void StackPush(ST *ps, STDataType x);
void StackPop(ST *ps);

STDataType StackTop(ST *ps);
int StackSize(ST* ps);

bool StackEmpty(ST* ps);





#endif //STACKQUEUE_STACK_H
