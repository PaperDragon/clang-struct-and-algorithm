//
// Created by SuperNu1L on 2022/8/21.
//

#ifndef STACKQUEUE_QUEUE_H
#define STACKQUEUE_QUEUE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "assert.h"


typedef int QDataType;

typedef struct QueueNode {
    struct QueueNode *next;
    QDataType data;
} QueueNode;

typedef struct Queue {
    struct QueueNode* head;
    struct QueueNode* tail;
}Queue;

void QueueInit(Queue *pq);
void QueueDestroy(Queue *pq);
void QueuePush(Queue *pq,QDataType x);
void QueuePop(Queue *pq);
bool QueueEmpty(Queue *pq);
QDataType QueueFront(Queue *pq);
QDataType QueueBack(Queue *pq);

int QueueSize(Queue *pq);

#endif //STACKQUEUE_QUEUE_H
