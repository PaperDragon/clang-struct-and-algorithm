#include "include/stack.h"
#include "include/queue.h"

void TestQueue();

void TestStack();


void TestQueue() {
    Queue q;
    QueueInit(&q);

    QueuePush(&q,5);
    QueuePush(&q,6);
    QueuePush(&q,7);
    QueuePop(&q);

    if (!QueueEmpty(&q)){
        QDataType front = QueueFront(&q);
        QDataType back = QueueBack(&q);
        printf("Head->data = %d,Tail->data = %d",front, back);
    }



    QueueDestroy(&q);
}

void TestStack() {
    ST st;
    StackInit(&st);
    StackPush(&st, 1);
    StackPush(&st, 2);
    StackPush(&st, 3);
    StackPop(&st);
    StackPush(&st, 4);
    StackPush(&st, 5);
    while (!StackEmpty(&st)) {
        printf("%d ", StackTop(&st));
        StackPop(&st);
    }

//    StackPop(&st);
//    StackPop(&st);
//    StackPop(&st);


    StackDesroy(&st);

}

int main() {
    TestQueue();
//    TestStack();
//    printf("Hello, World!\n");
    return 0;
}
