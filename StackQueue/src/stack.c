//
// Created by SuperNu1L on 2022/8/20.
//
#include "../include/stack.h"

void StackInit(ST *ps) {
    /*
     * 初始化时
     * ps->top = 0 表示指向栈顶数据的下一个
     * ps->top = -1 表示指向栈顶数据
     */
    assert(ps);
    ps->a = NULL;
    ps->top = 0;
    ps->capacity = 0;
}

void StackDesroy(ST *ps) {
    assert(ps);
    free(ps->a);
    ps->a=NULL;
    ps->capacity = ps->top = 0;
}

void StackPush(ST *ps, STDataType x) {
    assert(ps);
    if (ps->top == ps->capacity) {
        int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
        STDataType *tmp = realloc(ps->a, sizeof(STDataType) * newCapacity);
        if (tmp == NULL) {
            printf("realloc fail!\n");
            exit(-1);
        }
        ps->a = tmp;
        ps->capacity = newCapacity;
    }
    ps->a[ps->top] = x;
    ps->top++;
}

void StackPop(ST *ps) {
    assert(ps);
    ps->top--;
}

STDataType StackTop(ST *ps) {
    assert(ps);
    assert(!StackEmpty(ps));
    return ps->a[ps->top-1];
}

int StackSize(ST *ps) {
    assert(ps);
    return ps->top;
}

bool StackEmpty(ST *ps) {
    assert(ps);
    return ps->top == 0;
}

