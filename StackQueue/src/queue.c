//
// Created by SuperNu1L on 2022/8/21.
//

#include "../include/queue.h"

void QueueInit(Queue *pq) {
    assert(pq);
    pq->head = NULL;
    pq->tail = NULL;
}

void QueueDestroy(Queue *pq) {
    assert(pq);
    QueueNode *cur = pq->head;
    while (cur) {
        QueueNode *next = cur->next;
        free(cur);
        cur = next;
    }
    pq->head = pq->tail = NULL;
}

void QueuePush(Queue *pq, QDataType x) {
    assert(pq);
    // 先对新节点赋值
    QueueNode *newNode = (QueueNode *) malloc(sizeof(QueueNode));
    newNode->data = x;
    newNode->next = NULL;
    if (pq->head == NULL) {
        pq->head = pq->tail = newNode;
    } else {
        pq->tail->next = newNode;
        pq->tail = newNode;
    }

}

void QueuePop(Queue *pq) {
    /*
     * 从队列 head处删除数据
     */
    assert(!QueueEmpty(pq));

    QueueNode *next = pq->head->next;
    free(pq->head);
    pq->head = next;
    if (pq->head == NULL) {
        pq->tail = NULL;
    }

}

bool QueueEmpty(Queue *pq) {
    assert(pq);
    return pq->head == NULL;
}

QDataType QueueFront(Queue *pq) {
    assert(!QueueEmpty(pq));
    return pq->head->data;
}

QDataType QueueBack(Queue *pq) {
    assert(!QueueEmpty(pq));
    return pq->tail->data;
}

int QueueSize(Queue *pq) {
    assert(!QueueEmpty(pq));
    QueueNode *cur = pq->head;
    int n = 0;

    while (cur) {
        n++;
        cur = cur->next;
    }
    return n;
}

