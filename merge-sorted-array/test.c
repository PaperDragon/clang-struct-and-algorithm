//输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
//输出：[1,2,2,3,5,6]

#include "include/merge-sorted-array.h"

int main(void) {
    int nums1[] = {4, 5, 6,0,0,0};
    int m = 3;
    int nums2[] = {1,2,3};
    int n = 3;
//    int nums1[] = {1,2,3,0,0,0};
//    int m = 3;
//    int nums2[] = {4,5,6};
//    int n = 3;
    printArrayGroup(nums1, sizeof(nums1) / sizeof(int));
    merge(nums1, sizeof(nums1) / sizeof(int), m, nums2, sizeof(nums2) / sizeof(int), n);
    printArrayGroup(nums1, sizeof(nums1) / sizeof(int));

    return 0;
}