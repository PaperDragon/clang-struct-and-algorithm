//
// Created by SuperNu1L on 2022/8/5.
//

#ifndef MERGE_SORTED_ARRAY_MERGE_SORTED_ARRAY_H
#define MERGE_SORTED_ARRAY_MERGE_SORTED_ARRAY_H
#include <stdio.h>
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n);

void printArrayGroup(int *nums, int numSize) ;
#endif //MERGE_SORTED_ARRAY_MERGE_SORTED_ARRAY_H
