//
// Created by SuperNu1L on 2022/8/5.
//
#include "../include/merge-sorted-array.h"

void merge(int *nums1, int nums1Size, int m, int *nums2, int nums2Size, int n) {
    while (nums1Size > 0) {
        while (nums1Size > 0) {
            nums1Size--;
            if (n == 0) {
                nums1Size = 0;
            } else if (m == 0) {
                n--;
                nums1[nums1Size] = nums2[n];
            } else if (nums1[m - 1] > nums2[n - 1]) {
                m--;
                nums1[nums1Size] = nums1[m];
            } else {
                n--;
                nums1[nums1Size] = nums2[n];
            }
        }
}

void printArrayGroup(int *nums, int numSize) {
    for (int i = 0; i < numSize; i++) {
        printf("%d ", nums[i]);
    }
    printf("\n");
}