#include "include/seqlist.h"



int main(void) {
    SeqList s1;
    SeqListInit(&s1);
    SeqListPushBack(&s1, 10);
    SeqListPushBack(&s1, 2);
    SeqListPushBack(&s1, 3);
    SeqListPushBack(&s1, 4);

    SeqListPrint(&s1);

//    SeqListPopFront(&s1);
//    SeqListPopFront(&s1);
    printf("%d\n",SeqListFind(&s1, 10 ));
    SeqListPrint(&s1);

    SeqListDestory(&s1);
//    exit(0);
    return 0;
}

