//
// Created by SuperNu1L on 2022/8/2.
//

#ifndef SEQLIST_SEQLIST_H
#define SEQLIST_SEQLIST_H


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


typedef int SLDatatype;
#define N 100
typedef int SLDataType;  // SLDataType就是int

// // 静态顺序表
// 静态特点：如果满了就不让插入 缺点，不清楚定义多少空间
// N 太小不够用
// typedef struct SeqList{
//     SLDataType a[N];
//     int size; // 表示数组中存储了多少个数据
// } SeqList;


typedef struct SeqList {
    SLDataType *a;
    int size; // 表示数组中存储了多少个数据
    int capacity; // 数据实际能存数据空间容量多大（个数）
} SeqList;


// 接口函数 -- 命名风格跟着STL走
void SeqListInit(SeqList *ps); // 初始化
void SeqListDestory(SeqList *ps);  //删除
void SeqListPrint(SeqList *ps); // 打印
void SeqListPushBack(SeqList *ps, SLDataType x); // 尾插
void SeqListPopBack(SeqList *ps); // 尾删

void SeqListCheckCapacity(SeqList *ps); //检查是否存储空间不足
void SeqListPushFront(SeqList *ps, SLDataType x); // 头插
void SeqListPopFront(SeqList *ps); // 头删
int SeqListFind(SeqList *ps, SLDatatype x); //查找，没找到返回-1
void SeqListInsert(SeqList *ps, int pos, SLDatatype x); //插入
void SeqListEarse(SeqList *ps, int pos);

// ...


#endif //SEQLIST_SEQLIST_H
