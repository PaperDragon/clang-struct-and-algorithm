//
// Created by SuperNu1L on 2022/8/2.
//

#include "../include/seqlist.h"

void SeqListInit(SeqList *ps) {
    ps->a = NULL;
    ps->size = 0;
    ps->capacity = 0;
}

void SeqListDestory(SeqList *ps) {
    free(ps->a);
    ps->a = NULL;
    ps->size = ps->capacity = 0;
}

void SeqListPrint(SeqList *ps) {
    for (int i = 0; i < ps->size; ++i) {
        printf("%d  ", ps->a[i]);
    }
    printf("\n");
}

// 尾插
void SeqListPushBack(SeqList *ps, SLDataType x) {
//    SeqListCheckCapacity(ps);
//    ps->a[ps->size] = x;
//    ps->size++;
    SeqListInsert(ps, ps->size, x);
}

// 尾删
void SeqListPopBack(SeqList *ps) {
//    assert(ps->size > 0);
//    ps->size--;
    SeqListEarse(ps,ps->size-1);
}

void SeqListCheckCapacity(SeqList *ps) {
    if (ps->capacity == ps->size) { //如果没有空间或空间不足就扩容
        int newcapacity = ps->capacity * 2 == 0 ? 4 : ps->capacity * 2;
        SLDatatype *tmp = realloc(ps->a, newcapacity * sizeof(SLDatatype));
        if (tmp == NULL) {
            printf("realloc fail\n");
            exit(-1);
        }
        ps->a = tmp;
        ps->capacity = newcapacity;
    }
}


// 头插
void SeqListPushFront(SeqList *ps, SLDataType x) {
//    SeqListCheckCapacity(ps);
//    // 挪动数据
//    SLDatatype end = ps->size;
//    while (end >= 0) {
//        ps->a[end] = ps->a[end - 1];
//        --end;
//    }
//    ps->a[0] = x;
//    ps->size++;
    SeqListInsert(ps, 0, x);
}

// 头删
void SeqListPopFront(SeqList *ps) {
//    assert(ps->size > 0);
//    SLDatatype head = 0;
//    while (head <= ps->size - 2) {
//        ps->a[head] = ps->a[head + 1];
//        head++;
//    }
//    ps->size--;
    SeqListEarse(ps, 0);
}

//查找
int SeqListFind(SeqList *ps, SLDatatype x) {
    while (ps->size) {
        if (ps->a[ps->size - 1] == x) {
            return ps->size - 1;
        }
        ps->size--;
    }
    return -1;
}


//插入
void SeqListInsert(SeqList *ps, int pos, SLDatatype x) {
    assert(pos < ps->size && pos >= 0);
    SeqListCheckCapacity(ps);
    int end = ps->size - 1;
    while (end >= pos) {
        ps->a[end + 1] = ps->a[end];
        --end;
    }
    ps->a[pos - 1] = x;
}

//任意位置删除
void SeqListEarse(SeqList *ps, int pos) {
    assert(pos < ps->size && pos >= 0);
    int begin = pos + 1;
    while (begin < ps->size) {
        ps->a[begin - 1] = ps->a[begin];
        begin++;
    }
    ps->size--;
}