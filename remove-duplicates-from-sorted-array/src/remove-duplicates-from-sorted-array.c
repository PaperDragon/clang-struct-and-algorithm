//
// Created by SuperNu1L on 2022/8/5.
//
int removeDuplicates(int* nums, int numsSize){
    int dst = 0;
    int p = 0;
    if (numsSize == 0) return 0;
    while( numsSize > 0 ){
        if(nums[dst] == nums[p]) {
            p++;
        } else {
            nums[dst+1] = nums[p];
            dst++;
            p++;
        }
        numsSize--;
    }
    return dst+1;
}

void printArryGroup(int *nums, int numSize) {
    for(int i = 0;i < numSize;i++){
        printf("%d ",nums[i]);
    }
    printf("\n");
}