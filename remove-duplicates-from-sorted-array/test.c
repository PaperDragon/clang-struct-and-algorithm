//
// Created by SuperNu1L on 2022/8/5.
//
#include "include/remove-duplicates-from-sorted-array.h"


int main(void ){

    int a[] = {0,1,2,2,3,3,3,4,4,4,4,5,5,5,5,5};
    int  res = removeDuplicates(a, sizeof(a)/ sizeof(int ));
    printArryGroup(a,res);
    return 0;
}